---
title: "Activities"
weight: 2
header_menu: true
---

We mine. We build. We form fleets. We kill. We die. We laugh. We do it all over again. Come join us!

